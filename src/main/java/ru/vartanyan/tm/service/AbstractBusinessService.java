package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.api.IBusinessService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.AbstractBusinessEntity;
import ru.vartanyan.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    private final IBusinessRepository<E> businessRepository;

    public AbstractBusinessService(@NotNull final IBusinessRepository<E> businessRepository) {
        super(businessRepository);
        this.businessRepository = businessRepository;
    }

    @Nullable
    @Override
    public E findById(@Nullable final String id,
                      @NotNull final String userId) {
        if (id == null || id.isEmpty()) return null;
        return businessRepository.findById(id, userId);
    }

    @Override
    public void clear(@NotNull final String userId) {
        businessRepository.clear(userId);
    }

    @Override
    public void removeById(@Nullable final String id,
                           @Nullable final String userId) {
        if (id == null || id.isEmpty()) return;
        try {
            businessRepository.removeById(id, userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public List<E> findAll(@Nullable final Comparator comparator,
                           @Nullable final String userid) {
        if (comparator == null) return null;
        return businessRepository.findAll(comparator, userid);
    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final Integer index,
                            @Nullable final String userId) throws Exception {
        if (index < 0) throw new IncorrectIndexException(index);
        @Nullable final E entity = businessRepository.findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        return entity;
    }

    @Nullable
    @Override
    public E findOneByName(@Nullable final String name,
                           @Nullable final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = businessRepository.findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        return entity;
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index,
                                 @Nullable final String userId) throws Exception{
        if (index < 0) throw new IncorrectIndexException(index);
        @Nullable final E entity = businessRepository.findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        remove(entity);
    }

    @Override
    public void removeOneByName(@Nullable final String name,
                                @Nullable final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = businessRepository.findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        remove(entity);
    }

    @Override
    public void showEntity(@NotNull final E entity) {
        businessRepository.showEntity(entity);
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return businessRepository.findAll(userId);
    }

    @Override
    public void add(@NotNull String name,
                    @NotNull String description,
                    @NotNull String userId) throws Exception {

    }

    @Override
    public void updateEntityById(@Nullable final String id,
                                 @Nullable final String name,
                                 @Nullable final String description,
                                 @NotNull final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = findById(id, userId);
        if (entity == null) throw new NullObjectException();
        entity.setName(name);
        entity.setDescription(description);
    }

    @Override
    public void updateEntityByIndex(@NotNull final Integer index,
                                    @Nullable final String name,
                                    @Nullable final String description,
                                    @NotNull final String userId) throws Exception {
        if (index < 0) throw new IncorrectIndexException(index);
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        entity.setName(name);
        entity.setDescription(description);
    }

    @Override
    public void startEntityById(@Nullable final String id,
                                @NotNull final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = findById(id, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        entity.setDateStarted(dateStarted);
    }

    @Override
    public void startEntityByName(@Nullable final String name,
                                  @NotNull final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        entity.setDateStarted(dateStarted);
    }

    @Override
    public void startEntityByIndex(@NotNull final Integer index,
                                   @NotNull final String userId) throws Exception {
        if (index < 0) throw new IncorrectIndexException(index);
        @Nullable final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        entity.setDateStarted(dateStarted);
    }

    @Override
    public void finishEntityById(@Nullable final String id,
                                 @NotNull final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = findById(id, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
    }

    @Override
    public void finishEntityByName(@Nullable final String name,
                                   @NotNull final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
    }

    @Override
    public void finishEntityByIndex(@NotNull final Integer index,
                                    @NotNull final String userId) throws Exception {
        if (index < 0) throw new IncorrectIndexException(index);
        @Nullable final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
    }

    @Override
    public void updateEntityStatusById(@Nullable final String id,
                                       @NotNull final Status status,
                                       @NotNull final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = findById(id, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
    }

    @Override
    public void updateEntityStatusByName(@Nullable final String name,
                                         @NotNull final Status status,
                                         @NotNull final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
    }

    @Override
    public void updateEntityStatusByIndex(@NotNull final Integer index,
                                          @NotNull final Status status,
                                          @NotNull final String userId) throws Exception {
        if (index < 0) throw new IncorrectIndexException(index);
        @Nullable final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
    }

    public void showEntityByName(@Nullable final String name,
                                 @NotNull final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        businessRepository.showEntity(entity);
    }

}
