package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull protected final List<E> entities = new ArrayList<>();

    @Override
    public final void add(@NotNull final E entity) {
        entities.add(entity);
    }

    @Override
    public void remove(@Nullable final E entity) {
        entities.remove(entity);
    }

    @Override
    public void removeById(@NotNull final String id) throws Exception {
        remove(entities.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null)
        );
    }

    @Nullable
    @Override
    public final E findById(@NotNull final String id) {
        return entities.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public void clear() {
        entities.clear();
    }

    public void addAll(final List<E> list) {
        if (list == null) return;
        entities.addAll(list);
    }

}
