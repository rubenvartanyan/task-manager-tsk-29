package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-show-by-id";
    }

    @Override
    public String description() {
        return "Show task by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().findById(id, userId);
        serviceLocator.getTaskService().showEntity(task);
    }

}
