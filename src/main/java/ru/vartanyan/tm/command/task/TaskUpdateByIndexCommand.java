package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-update-by-index";
    }

    @Override
    public String description() {
        return "Update task by index";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER INDEX]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskService().findOneByIndex(index, userId);
        System.out.println("[ENTER NAME]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[INTER DESCRIPTION]");
        @NotNull final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateEntityByIndex(index, name, description,userId);
        System.out.println("[TASK UPDATED]");
    }

}
