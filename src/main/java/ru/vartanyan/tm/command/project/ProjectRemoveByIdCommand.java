package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove project by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE PROJECT");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().removeById(id, userId);
        System.out.println("[PROJECT REMOVED]");
    }

}
