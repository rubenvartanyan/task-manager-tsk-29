package ru.vartanyan.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Domain;
import ru.vartanyan.tm.enumerated.Role;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;

public class DataJsonSaveFasterXmlCommand extends AbstractDataCommand{

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-fasterxml-json-save";
    }

    @Nullable
    @Override
    public String description() {
        return "Save data from JSON fasterxml";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON FASTERXML SAVE]");
        final Domain domain = getDomain();
        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_JSON);
        fileOutputStream.write(json.getBytes(StandardCharsets.UTF_8));
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
