package ru.vartanyan.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Domain;
import ru.vartanyan.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public class DataXmlSaveJaxBCommand extends AbstractDataCommand{

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-jaxb-save";
    }

    @Nullable
    @Override
    public String description() {
        return "Save data from XML jaxb";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML JAXB SAVE]");
        final Domain domain = getDomain();
        final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JAXB_XML);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();

    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
