package ru.vartanyan.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Domain;
import ru.vartanyan.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlLoadFasterXmlCommand extends AbstractDataCommand{

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-fasterxml-load";
    }

    @Nullable
    @Override
    public  String description() {
        return "Load data from XML fasterxml";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML FASTERXML LOAD]");
        final String xml = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_XML)));
        final ObjectMapper objectMapper = new XmlMapper();
        final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
