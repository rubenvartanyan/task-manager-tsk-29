package ru.vartanyan.tm.companent;

import lombok.SneakyThrows;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.command.data.BackupLoadCommand;
import static ru.vartanyan.tm.command.data.AbstractDataCommand.BACKUP_SAVE;
import static ru.vartanyan.tm.command.data.AbstractDataCommand.BACKUP_LOAD;

public class Backup extends Thread {

    private static final int INTERVAL = 15000;

    private final Bootstrap bootstrap;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    @SneakyThrows
    @Override
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    @SneakyThrows
    public void save() {
        bootstrap.parseCommand(BACKUP_SAVE);
    }

    @SneakyThrows
    public void load() {
        bootstrap.parseCommand(BACKUP_LOAD);
    }

    public void init() {
        load();
        start();
    }

}
