package ru.vartanyan.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.other.ISaltSetting;

import java.nio.charset.StandardCharsets;

public interface HashUtil {

   @Nullable
    static String salt(@Nullable final ISaltSetting setting,
                       @Nullable String value) {
        if (setting == null) return null;
        @Nullable final String secret = setting.getPasswordSecret();
        @Nullable final Integer iteration = setting.getPasswordIteration();
        return salt(secret, value, iteration);


    }

    @Nullable
    static String salt(@Nullable final String value,
                       @Nullable final String secret,
                       @Nullable final Integer iteration) {
        if (value == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);

        }
        return result;
    }

    @Nullable
    static String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            @NotNull final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes(StandardCharsets.UTF_8));
            @NotNull final StringBuilder sb = new StringBuilder();
            for (int i = 1; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
